/*
 * Copyright 2020 Nick Verlinden (https://nickverlinden.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let doc = document;
let html = doc.documentElement;
let sw = navigator.serviceWorker;

// remove javascript warning
html.removeAttribute('require-javascript');

//
// install service worker for offline access
//
async function enableOffline() 
{
    if (!(await sw.getRegistration())) 
    {
        sw.register("offline.js")
          .then()
          .catch(error => console.log("ServiceWorker registration failed: ", err));
        sw.ready.then(() => { window.location.reload(); });
    }
}

//
// displays a very user unfriendly blocking message
//
function message(text, icon) 
{
    if (text) 
    {
        doc.body.setAttribute('blocking-message', text);
    }
    else
    {
        doc.body.setAttribute('blocking-message', '');
    }
    if (icon) 
    {
        doc.body.setAttribute('blocking-message-icon', icon);
    }
}

//
// displays a very user unfriendly blocking progress bar
//
function progress(percent, text, icon) 
{
    if (percent != null) 
    {
        let steps = 20;
        let step = Math.floor(steps * (percent / 100));
        //
        // unicode characters
        //  '\u00a0' white space
        //  '\u00b7' middle dot
        //
        //doc.body.setAttribute('blocking-progress', '\u2588'.repeat(step) + '\u00a0'.repeat(Math.ceil(steps - step)) + '\u2588');
        doc.body.setAttribute('blocking-progress', '/'.repeat(step) + ' '.repeat(Math.ceil(steps - step)) + '');
    }
    else
    {
        doc.body.setAttribute('blocking-progress', '');
    }

    if (text) 
    {
        doc.body.setAttribute('blocking-progress-message', text);
        doc.body.setAttribute('blocking-progress-icon', '');
    }
    else 
    {
        doc.body.setAttribute('blocking-progress-message', '');
    }

    if (icon) 
    {
        doc.body.setAttribute('blocking-progress-icon', icon);
    }
}

//
// removes the opaque overlay previously added by setting the block attribute on the body element
//
function unblock() 
{
    doc.body.removeAttribute('block');
}

// install service-worker for caching offline resources
if (html.getAttribute('require-offline') !== null)
{
    enableOffline();
}